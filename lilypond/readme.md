# Tabulatury programu LilyPond

W tym katalogu znajdują się źródła dla tabulatur w programie *LilyPond* (`.ly`) oraz już skompilowane pliki (`.pdf`, skrypty nie automatyzują ich kompilacji). Poniższe instrukcja na wypadek potrzeby rekompilacji.

## Kompilacja (Windows)

Wymagany jest program *LilyPond* dostępny [tutaj](http://lilypond.org/download.html).

W tym katalogu (lilypond) wykonaj:
```
& "C:\Program Files (x86)\LilyPond\usr\bin\lilypond" --pdf *.ly
```
Gdzie `&` umożliwia wykonanie programu spod ścieżki ze znakami specjalnymi (spacje).

## Przydatna dokumentacja

Po kliknięciu na obrazki w dokumentacji wyświetlane jest źródło.

* [Przykłady dla instrumentów strunowych](https://lilypond.org/doc/v2.19/Documentation/notation/common-notation-for-fretted-strings)
* [Predefiniowane schematy akordów](http://lilypond.org/doc/v2.19/Documentation/notation/predefined-fretboard-diagrams)
* [Źródło predefiniowanych schematów akordów](https://github.com/lilypond/lilypond/blob/master/ly/predefined-guitar-fretboards.ly)
