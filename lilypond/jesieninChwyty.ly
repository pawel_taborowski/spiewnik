\include "lilypond-book-preamble.ly"
\include "predefined-guitar-fretboards.ly"

\paper {
  indent = 0\mm
  line-width = 160\mm
  % offset the left padding, also add 1mm as lilypond creates cropped
  % images with a little space on the right
  line-width = #(- line-width (* mm  3.000000) (* mm 1))
  line-width = 160\mm - 2.0 * 10.16\mm
  % offset the left padding, also add 1mm as lilypond creates cropped
  % images with a little space on the right
  line-width = #(- line-width (* mm  3.000000) (* mm 1))
  ragged-right = ##t
}

% podmieniony As0, oryginalny byl za wysoko na gryfie i zle brzmial
\storePredefinedDiagram #default-fret-table \chordmode {aes:dim}
                        #guitar-tuning
                        "4-2;2-1;o;4-3;o;4-4;"

chordsline = \chordmode {
  bes:dim as:dim
}

\score {
  <<
    \new ChordNames {
      \chordsline
    }
    \new FretBoards {
      \chordsline
    }
  >>
  \layout {
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
  }
}
