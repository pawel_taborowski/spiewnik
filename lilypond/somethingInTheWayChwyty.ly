\include "lilypond-book-preamble.ly"
\include "predefined-guitar-fretboards.ly"

\paper {
  indent = 0\mm
  line-width = 160\mm
  % offset the left padding, also add 1mm as lilypond creates cropped
  % images with a little space on the right
  line-width = #(- line-width (* mm  3.000000) (* mm 1))
  line-width = 160\mm - 2.0 * 10.16\mm
  % offset the left padding, also add 1mm as lilypond creates cropped
  % images with a little space on the right
  line-width = #(- line-width (* mm  3.000000) (* mm 1))
  ragged-right = ##t
}

\storePredefinedDiagram #default-fret-table \chordmode {c/g}
                        #guitar-tuning
                        "3-3;3-4;2-2;o;1-1;o;"

chordsline = \chordmode {
  c/g
}

\score {
  <<
    \new ChordNames {
      \chordsline
    }
    \new FretBoards {
      \chordsline
    }
  >>
  \layout {
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
  }
}
