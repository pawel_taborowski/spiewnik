# Śpiewnik

Śpiewnik przygotowany jest w dwóch formatach:
* A4 - `KaczmarskiA4.tex`
* ekran Kindle - `KaczmarskiKindle.tex`

## Pobierz

* [A4](https://bitbucket.org/pawel_taborowski/spiewnik/downloads/KaczmarskiA4.pdf)
* [Kindle](https://bitbucket.org/pawel_taborowski/spiewnik/downloads/KaczmarskiKindle.pdf)

## Kompilacja

Kompilacja przetestowana w pakiecie *MikTeX 2.9* dla Windowsa i *TeX Live 2021* dla Linuksa. W aktualnych wersjach buduje się źle (stan na lipiec 2022; tekst jest nieprawidłowo składany).

Wersja Kindle
```bash
pdflatex KaczmarskiKindle.tex
songidx -l pl_PL idxKaczmarskiKindle.sxd idxKaczmarskiKindle.sbx
pdflatex KaczmarskiKindle.tex
```

Wersja A4
```bash
pdflatex KaczmarskiA4.tex
songidx -l pl_PL idxKaczmarskiA4.sxd idxKaczmarskiA4.sbx
pdflatex KaczmarskiA4.tex
```

### Visual Studio Code

Przetestowane oprogramowanie (na Windows 10):
* *Visual Studio Code* 1.69.2
    * wtyczka *LaTeX Workshop* v8.28.0
* *Docker Desktop* v4.10.1
    * WSL 2
    * obraz *texlive/texlive:TL2021-historic* 6e5fd07eaec0

W ustawieniach wtyczki zaznaczyć opcję `Docker` a w polu `Docker>Image` wpisać: `texlive/texlive:TL2021-historic`. Domyślna komenda budująca jest OK, ale nie generuje indeksu alfabetycznego.

### songidx
`songidx` jest skryptem *Lua* dostarczanym wraz z niektórymi dystrybucjami *LaTeXa*, wg. dokumentacji wywołanie powinno miec postać `texlua songidx.lua idxfile.sxd idxfile.sbx`, jednak w *MiKTeX* jest udostępniany bezpośrednio. Przełącznik `-l pl_PL` ustawia locale (do prawidłowej kolejności sortowania spisu treści), podana wartość jest prawidłowa dla Windowsa.

### LilyPond

Szczegóły dotyczące tabulatur w *LilyPond* oraz ich kompilacji znajdują się w [osobnym readme](./lilypond/readme.md).

## Przydatna dokumentacja

* [*LaTeX* for Musicians](http://tug.ctan.org/info/latex4musicians/latex4musicians.pdf)
* [The *songs* package](https://sunsite.icm.edu.pl/pub/CTAN/macros/latex/contrib/songs/songs.pdf)
* [*MusiXTeX* (18.10 Tablature)](https://sunsite.icm.edu.pl/pub/CTAN/macros/musixtex/doc/musixdoc.pdf)
